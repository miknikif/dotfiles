{
  description = "Main flake";
  inputs = {
    # Giant monorepo with recipes called derivations that say how to build software
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.05";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05"; # Can be nixpkgs-unstable

    # Manages configs links things into you home directory
    home-manager.url = "github:nix-community/home-manager/release-23.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # Controls system level software and settings including fonts
    darwin.url = "github:lnl7/nix-darwin";
    darwin.inputs.nixpkgs.follows = "nixpkgs";

    # # Tool to make mac aliases without needing Finder scripting permissions for home-manager app linking
    # mkalias.url = "github:reckenrode/mkalias";
    # mkalias.inputs.nixpkgs.follows = "nixpkgs";

    # nvim with LSP
    pwnvim.url = "git+https://xaked.com/xaked/nvim.git";
    pwnvim.inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = inputs @ {
    nixpkgs-unstable,
    nixpkgs-stable,
    nixpkgs,
    home-manager,
    darwin,
    pwnvim,
    ...
  }: let
    inherit (home-manager.lib) homeManagerConfiguration;

    mkPkgs = system:
      import nixpkgs {
        inherit system;
        inherit
          (import ./modules/overlays.nix {
            inherit inputs nixpkgs-stable nixpkgs-unstable;
          })
          overlays
          ;
        config = import ./config.nix;
      };

    mkHome = username: workspaces: modules: {
      users.users."${username}".home = "/Users/${username}";
      home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        backupFileExtension = "bak";
        extraSpecialArgs = {
          inherit inputs workspaces;
        };
        users."${username}".imports = modules;
      };
    };
  in {
    darwinConfigurations.mbp1915 = let
      username = "miknikif";
      workspaces = ["private"]; # First workspace will be considered as the main one
    in
      darwin.lib.darwinSystem {
        system = "x86_64-darwin";
        pkgs = mkPkgs "x86_64-darwin";
        modules = [
          ./modules/darwin
          ./modules/darwin/brew-private.nix
          home-manager.darwinModules.home-manager
          (
            mkHome username workspaces [
              ./modules/home-manager
              ./modules/home-manager/private.nix
            ]
          )
        ];
      };

    darwinConfigurations.mbp1916 = let
      username = "miknikif";
      workspaces = ["work/tp"]; # First workspace will be considered as the main one
    in
      darwin.lib.darwinSystem {
        system = "x86_64-darwin";
        pkgs = mkPkgs "x86_64-darwin";
        modules = [
          ./modules/darwin
          ./modules/darwin/brew-tp.nix
          home-manager.darwinModules.home-manager
          (
            mkHome username workspaces [
              ./modules/home-manager
              ./modules/home-manager/work-tp.nix
            ]
          )
        ];
      };

    darwinConfigurations.mbp2116 = let
      username = "miknikif";
      workspaces = ["work/tp"]; # First workspace will be considered as the main one
    in
      darwin.lib.darwinSystem {
        system = "aarch64-darwin";
        pkgs = mkPkgs "aarch64-darwin";
        modules = [
          ./modules/darwin
          ./modules/darwin/brew-tp.nix
          home-manager.darwinModules.home-manager
          (
            mkHome username workspaces [
              ./modules/home-manager
              ./modules/home-manager/work-tp.nix
            ]
          )
        ];
      };

    darwinConfigurations.D19JHXHNW9 = let
      username = "mkravts";
      workspaces = ["work/softserve"]; # First workspace will be considered as the main one
    in
      darwin.lib.darwinSystem {
        system = "aarch64-darwin";
        pkgs = mkPkgs "aarch64-darwin";
        modules = [
          {homebrew.brewPrefix = "/opt/homebrew/bin";}
          ./modules/darwin
          ./modules/darwin/brew-ss.nix
          home-manager.darwinModules.home-manager
          (
            mkHome username workspaces [
              ./modules/home-manager
              ./modules/home-manager/work-ss.nix
            ]
          )
        ];
      };
  };
}
