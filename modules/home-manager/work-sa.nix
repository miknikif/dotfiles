{pkgs, ...}: {
  home.file.".gitconfig_sa".source = ./dotfiles/git/gitconfig_sa;
  # Don"t change this when you change package input. Leave it alone.
  programs.zsh.shellAliases = {
    # Prod account aliases;
    titanprodeast = "export KUBECONFIG=/Users/mkravts/.kube/secureauth/kubeconfig_titan-prod-us-east-1; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    titanprodwest = "export KUBECONFIG=/Users/mkravts/.kube/secureauth/kubeconfig_titan-prod-us-west-2; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    titandeveast = "export KUBECONFIG=/Users/mkravts/.kube/secureauth/kubeconfig_titan-dev-us-east-1; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    titandevwest = "export KUBECONFIG=/Users/mkravts/.kube/secureauth/kubeconfig_titan-dev-us-west-2; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    polarisdeveast = "export KUBECONFIG=/Users/mkravts/.kube/secureauth/kubeconfig_sa-polaris-dev-us-east-1; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    polarisprodeast = "export KUBECONFIG=/Users/mkravts/.kube/secureauth/kubeconfig_sa-polaris-prod-us-east-1; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    polarisdevwest = "export KUBECONFIG=/Users/mkravts/.kube/secureauth/kubeconfig_sa-polaris-dev-us-west-2; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    arculixdevprimary = "export KUBECONFIG=/Users/mkravts/work/soft_serve/secureauth/acceptto-corp/arculix/.artifacts/arculix-dev/primary/infra/kubeconfig; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    arculixstageprimary = "export KUBECONFIG=/Users/mkravts/work/soft_serve/secureauth/acceptto-corp/arculix/.artifacts/arculix-stage/primary/infra/kubeconfig; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    arculixstagedr1 = "export KUBECONFIG=/Users/mkravts/work/soft_serve/secureauth/acceptto-corp/arculix/.artifacts/arculix-stage/dr1/infra/kubeconfig; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    arculixprodprimary = "export KUBECONFIG=/Users/mkravts/work/soft_serve/secureauth/acceptto-corp/arculix/.artifacts/arculix-prod/primary/infra/kubeconfig; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    arculixproddr1 = "export KUBECONFIG=/Users/mkravts/work/soft_serve/secureauth/acceptto-corp/arculix/.artifacts/arculix-prod/dr1/infra/kubeconfig; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    kubelocal = "export KUBECONFIG=/Users/mkravts/.kube/config; export KUBE_CONFIG_PATH=\${KUBECONFIG}";
    # AWS profile aliases;
    sa-ctl = "export AWS_PROFILE=sa-ctl";
    sa-stg = "export AWS_PROFILE=sa-stg";
    sa-stage-aws704 = "export AWS_PROFILE=sa-stage-aws704";
    sa-dev = "export AWS_PROFILE=sa-dev";
    sa-dev-sso = "export AWS_PROFILE=sa-dev-sso";
    sa-dev-aws304 = "export AWS_PROFILE=sa-dev-aws304";
    sa-prod = "export AWS_PROFILE=sa-prod";
    sa-prod-sso = "export AWS_PROFILE=sa-prod-sso";
    sa-tools = "export AWS_PROFILE=sa-tools";
    saas-idp = "export AWS_PROFILE=saas-idp";
    docker = "podman";
  };
  home.packages = with pkgs; [
    blackbox
    awscli2
    go
  ];
}
