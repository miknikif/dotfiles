{pkgs, ...}: {
  home.file.".gitconfig_tp".source = ./dotfiles/git/gitconfig_tp;
  home.packages = with pkgs; [
    ansible
    vagrant
    packer
    go
  ];
  programs.zsh.shellAliases = {
    vpnotp = "op item get 5kpxjqtvxnqjgdecbed3lylxoa --otp";
    ovpn = "sudo /usr/local/opt/openvpn/sbin/openvpn --config ~/ovpn/mito.ovpn";
  };
}
