{
  pkgs,
  lib,
  workspaces,
  ...
}: {
  home.stateVersion = "23.05";
  home.packages = with pkgs; [
    ripgrep
    blackbox
    fd
    curl
    coreutils-full
    gawk
    less
    git
    gnupg
    pinentry_mac
    ansible
    sshpass
    reattach-to-user-namespace
    oath-toolkit
    htop
    jq
    yq
    kind # Kubernetes in Docker
    pluto # Check kubernetes for depricated APIs
    podman-compose
    pkgs.pwnvim
    bats
    unixtools.watch
    nmap
    qemu
    podman
    # TODO: check if we can install 1password system-wide (probably with darwin module)
    # _1password # cli tool
    pipenv # Better python venv tool
    tree # Command to produce a depth indented directory listing
  ];

  home.sessionVariables = {
    PAGER = "less -RF";
    CLICLOLOR = 1;
    EDITOR = "nvim";
  };

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    enableAutosuggestions = true;
    enableSyntaxHighlighting = true;
    historySubstringSearch.enable = true;
    shellAliases = {
      ls = "ls --color=auto -F";
      nixswitch = "darwin-rebuild switch --flake ~/.df/.#";
      nixlg = "darwin-rebuild --list-generations --flake ~/.df/.#";
      nixrb = "darwin-rebuild --rollback --flake ~/.df/.#";
      nixup = "pushd ~/.df; nix flake update; nixswitch; popd";
      nixclean = "nix-collect-garbage --delete-older-than 14d";
    };
    envExtra = ''
      export TF_PLUGIN_CACHE_DIR="$HOME/.terraform.d/plugin-cache";
      export SSH_AUTH_SOCK="$(gpgconf --list-dirs | grep ssh | tr ":" "\n" | tail -n-1)";
      export GPG_TTY=$(tty);
      # TODO: check why programs.fzf.colors are not applied
      # FZF Dracula theme
      export FZF_DEFAULT_OPTS='--color=dark --color=fg:-1,bg:-1,hl:#5fff87,fg+:-1,bg+:-1,hl+:#ffaf5f --color=info:#af87ff,prompt:#5fff87,pointer:#ff87d7,marker:#ff87d7,spinner:#ff87d7';
    '';
  };

  # Command prompt
  programs.starship = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
  };

  home.file.".inputrc".source = ./dotfiles/inputrc;

  # GNUPG
  home.file.".gnupg/gpg.conf".text = ''
    keyserver hkps://keys.openpgp.org
    use-agent
  '';
  home.file.".gnupg/scdaemon.conf".text = ''
    # Source: https://github.com/DataDog/yubikey/blob/master/env.sh#L59
    disable-ccid
    reader-port "Yubico YubiKey OTP+FIDO+CCID"
  '';
  home.file.".gnupg/gpg-agent.conf".text = ''
    pinentry-program ${pkgs.pinentry_mac}/Applications/pinentry-mac.app/Contents/MacOS/pinentry-mac
    enable-ssh-support
    default-cache-ttl 600
    default-cache-ttl-ssh 600
    max-cache-ttl 7200
    max-cache-ttl-ssh 7200
  '';

  # Git
  home.file.".gitconfig".source = ./dotfiles/git/gitconfig;

  home.activation.symlinks =
    lib.mkIf pkgs.stdenv.hostPlatform.isDarwin
    (lib.hm.dag.entryAfter ["writeBoundary"] ''
      dir_list="$HOME/.ssh                                        $HOME/cloud/${builtins.head workspaces}/.ssh           d
                $HOME/.ssh_pass                                   $HOME/cloud/${builtins.head workspaces}/.ssh_pass      d
                $HOME/.totp                                       $HOME/cloud/${builtins.head workspaces}/.totp          d
                $HOME/.aws                                        $HOME/cloud/${builtins.head workspaces}/.aws           d
                $HOME/.kube                                       $HOME/cloud/${builtins.head workspaces}/.kube          d
                $HOME/.gcloud                                     $HOME/cloud/${builtins.head workspaces}/.gcloud        d
                $(echo -e "${builtins.concatStringsSep "\\n" (builtins.map (x: "$HOME/workspace/" + x + "        $HOME/cloud/" + x + "/workspace        d") workspaces)}")"

      OIFS="$IFS"
      IFS=$'\n'
      for link in $dir_list
      do
        _link_dst=$(echo $link | ${pkgs.gawk}/bin/gawk '{print $1}')
        _link_src=$(echo $link | ${pkgs.gawk}/bin/gawk '{print $2}')
        _link_type=$(echo $link | ${pkgs.gawk}/bin/gawk '{print $3}')

        echo "$_link_src => $_link_dst ($_link_type)"
        $DRY_RUN_CMD [ "$_link_type" == "d" ] && [ ! -L "$_link_dst" ] && mkdir -p "$(dirname "$_link_dst")"
        $DRY_RUN_CMD [ "$_link_type" == "d" ] && [ ! -d "$_link_src" ] && mkdir -p "$_link_src"
        $DRY_RUN_CMD [ -L "$_link_dst" ] && unlink "$_link_dst"
        $DRY_RUN_CMD ln -sfT "$_link_src" "$_link_dst"
      done
      IFS="$OIFS"
    '');
}
