{...}: {
  imports = [
    ./core.nix
    ./pkgs/fzf
    ./pkgs/bat
    ./pkgs/alacritty
    ./pkgs/tmux
    ./pkgs/task
  ];
}
