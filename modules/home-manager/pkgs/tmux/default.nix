{pkgs, ...}: {
  programs.tmux = {
    enable = true;
    shell = "${pkgs.zsh}/bin/zsh";
    aggressiveResize = true;
    historyLimit = 50000;
    keyMode = "vi";
    mouse = true;
    newSession = false;
    terminal = "screen-256color";
    escapeTime = 0;
    plugins = with pkgs; [
      # TODO: restore this when plugin v2.2.0 would be available in the nixpkgs
      # {
      #   plugin = tmuxPlugins.dracula;
      #   extraConfig = ''
      #     set -g @dracula-show-flags true
      #     set -g @dracula-show-powerline true
      #     set -g @dracula-refresh-rate 10
      #     set -g @dracula-show-left-icon session
      #     set -g @dracula-show-empty-plugins false
      #     set -g @dracula-cpu-usage-label "L"
      #     set -g @dracula-cpu-display-load false
      #     set -g @dracula-ram-usage-label "M"
      #     set -g @dracula-ping-server "1.1.1.1"
      #     set -g @dracula-ping-rate 5
      #     set -g @dracula-day-month true
      #     set -g @dracula-show-fahrenheit false
      #     set -g @dracula-show-location false
      #     set -g @dracula-plugins "cpu-usage ram-usage weather time"
      #   '';
      # }
      {
        plugin = tmuxPlugins.yank;
        extraConfig = ''
          set -g default-command "reattach-to-user-namespace -l $SHELL"
          bind-key -T copy-mode-vi v send-keys -X begin-selection
          bind-key -T copy-mode-vi y send-keys -X copy-pipe "reattach-to-user-namespace pbcopy"

          #--------------------------------------------------------------------------------------------
          # TMUX Dracula theme
          #--------------------------------------------------------------------------------------------

          set -g @dracula-show-flags true
          set -g @dracula-show-powerline true
          set -g @dracula-refresh-rate 15
          set -g @dracula-show-left-icon session
          set -g @dracula-show-empty-plugins false
          set -g @dracula-cpu-usage-label "🖥"
          set -g @dracula-cpu-display-load true
          set -g @dracula-ram-usage-label "💾"
          set -g @dracula-ping-server "google.com"
          set -g @dracula-ping-rate 10
          set -g @dracula-day-month true
          set -g @dracula-show-fahrenheit false
          set -g @dracula-show-location false
          set -g @dracula-show-timezone true
          set -g @dracula-plugins "cpu-usage ram-usage weather network-ping time battery"
          run-shell ~/.config/tmux/plugins/tmux-theme-dracula/dracula.tmux
        '';
      }
      {
        plugin = tmuxPlugins.vim-tmux-navigator;
        extraConfig = ''
          # Shift alt vim keys to switch windows
          bind -n M-H previous-window
          bind -n M-L next-window
        '';
      }
      {
        plugin = tmuxPlugins.resurrect;
        extraConfig = "set -g @resurrect-strategy-nvim 'session'";
      }
      {
        plugin = tmuxPlugins.continuum;
        extraConfig = ''
          set -g @continuum-restore 'on'
          set -g @continuum-save-interval '60' # minutes
        '';
      }
    ];
    extraConfig = ''
      # Fix colors inside tmux
      set-option -sa terminal-overrides ",xterm*:Tc"

      # Rebind prefix key to ctrl-space
      unbind C-b
      set -g prefix C-Space
      bind C-Space send-prefix

      # Start windows and panes at 1, not 0
      set -g base-index 1
      set -g pane-base-index 1
      set-window-option -g pane-base-index 1
      set-option -g renumber-windows on

      # Open panes in current directory
      bind '"' split-window -v -c "#{pane_current_path}"
      bind % split-window -h -c "#{pane_current_path}"
    '';
  };
  home.file = {
    ".config/tmux/plugins/tmux-theme-dracula".source = pkgs.fetchFromGitHub {
      owner = "dracula";
      repo = "tmux";
      rev = "v2.2.0";
      sha256 = "sha256-9p+KO3/SrASHGtEk8ioW+BnC4cXndYx4FL0T70lKU2w=";
    };
  };
}
