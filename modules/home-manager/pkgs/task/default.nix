{
  lib,
  pkgs,
  workspaces,
  ...
}: {
  home.packages = with pkgs; [
    tasksh
    timewarrior # timetracking
    taskwarrior-tui # TUI for taskwarrior (task manager)
    # taskwarrior-time-tracking-hook # timetracking for taskwarrior
  ];

  # taskwarrior
  programs.taskwarrior = {
    enable = true;
    dataLocation = "$HOME/cloud/${builtins.head workspaces}/.task";
    config = {
      news.version = "2.6.0";
      weekstart = "monday";
    };
    extraConfig = ''
      include ${pkgs.taskwarrior}/share/doc/task/rc/dark-violets-256.theme
    '';
  };

  home.activation.taskwarrior-timewarrior = lib.hm.dag.entryAfter ["writeBoundary"] ''
    $DRY_RUN_CMD rm -rf "$HOME/cloud/${builtins.head workspaces}/.task/hooks"
    $DRY_RUN_CMD mkdir -p "$HOME/cloud/${builtins.head workspaces}/.task/hooks"
    $DRY_RUN_CMD cp "${pkgs.timewarrior}/share/doc/timew/ext/on-modify.timewarrior" "$HOME/cloud/${builtins.head workspaces}/.task/hooks/on-modify.timewarrior"
    $DRY_RUN_CMD chmod +x "$HOME/cloud/${builtins.head workspaces}/.task/hooks/on-modify.timewarrior"
    $DRY_RUN_CMD ln -sf "$HOME/.config/task/taskrc" "$HOME/.taskrc"
    $DRY_RUN_CMD mkdir -p "$HOME/cloud/${builtins.head workspaces}/.timewarrior/"
    $DRY_RUN_CMD ln -sfT "$HOME/cloud/${builtins.head workspaces}/.timewarrior" "$HOME/.timewarrior"
  '';
}
