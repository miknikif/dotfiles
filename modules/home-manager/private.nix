{pkgs, ...}: {
  home.file.".gitconfig_personal".source = ./dotfiles/git/gitconfig_personal;
  home.packages = with pkgs; [
    go
  ];
}
