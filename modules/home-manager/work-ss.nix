{pkgs, ...}: {
  home.file.".gitconfig_ss".source = ./dotfiles/git/gitconfig_ss;
  home.packages = with pkgs; [
    go
  ];
}
