{
  inputs,
  nixpkgs-unstable,
  ...
}: {
  overlays = [
    (final: prev: {
      unstable = import nixpkgs-unstable {
        inherit (prev) system;
        config = import ../config.nix;
        nix.package = inputs.nixos-unstable.nix;
      };
    })
    (final: prev: {
      # inherit (inputs.mkalias.packages.${final.system}) mkalias;
      inherit (inputs.pwnvim.packages.${final.system}) pwnvim;
    })
    # (final: prev: {
    #   taskwarrior-time-tracking-hook = prev.python3Packages.buildPythonPackage rec {
    #     pname = "taskwarrior-time-tracking-hook";
    #     version = "0.1.4";
    #     format = "setuptools";

    #     src = prev.fetchPypi {
    #       inherit pname version;
    #       hash = "sha256-N+b0Ke2glqLV+D7DvcG8x0qh4JGi8iP0o4Laf/6vDSg=";
    #     };
    #     propagatedBuildInputs = [
    #       (prev.python3Packages.buildPythonPackage rec {
    #         pname = "taskw";
    #         version = "2.0.0";
    #         format = "setuptools";

    #         src = prev.fetchPypi {
    #           inherit pname version;
    #           hash = "sha256-EQm9+b3nqbMqUAejAsh4MD/2UYi2QiWsdKMomkxUi90=";
    #         };
    #         propagatedBuildInputs = with prev.python310Packages; [
    #           kitchen
    #           python-dateutil
    #           pytz
    #         ];
    #         doCheck = false;
    #       })
    #     ];
    #     doCheck = false;
    #   };
    # })
  ];
}
