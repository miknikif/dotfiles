{pkgs, ...}: {
  system = {
    # backwards compat; don't change
    stateVersion = 4;
    defaults = {
      LaunchServices = {
        LSQuarantine = true;
      };
      loginwindow = {
        # disable guest account
        GuestEnabled = false;
        # Show name instead of username
        SHOWFULLNAME = false;
        # Disables the ability for a user to access the console by typing ">console" for a username at the login window
        DisableConsoleAccess = true;
      };
      finder = {
        AppleShowAllExtensions = true;
        _FXShowPosixPathInTitle = true;
        FXEnableExtensionChangeWarning = false;
        QuitMenuItem = true;
      };
      trackpad = {
        # silent clicking = 0, default = 1
        ActuationStrength = 1;
        # enable tap to click
        Clicking = false;
        # firmness level, 0 = lightest, 2 = heaviest
        #FirstClickThreshold = 1;
        # firmness level for force touch
        #SecondClickThreshold = 1;
        # don't allow positional right click
        #TrackpadRightClick = false;
        # three finger drag for space switching
        # TrackpadThreeFingerDrag = true;
      };
      dock = {
        autohide = true;
        autohide-delay = 0.0;
        autohide-time-modifier = 0.2;
        expose-animation-duration = 0.2;
        tilesize = 48;
        launchanim = false;
        static-only = false;
        showhidden = true;
        show-recents = false;
        show-process-indicators = true;
        orientation = "bottom";
        mru-spaces = false;
        # Corner actions
        # bl - bottom left
        # br - bottom right
        # tl - top left
        # tr - top right
        # Available options:
        # 1: Disabled
        # 2: Mission Control
        # 3: Application Windows
        # 4: Desktop
        # 5: Start Screen Saver
        # 6: Disable Screen Saver
        # 7: Dashboard
        # 10: Put Display to Sleep
        # 11: Launchpad
        # 12: Notification Center
        # 13: Lock Screen
        # 14: Quick Note
        wvous-bl-corner = 1;
        wvous-br-corner = 1;
        wvous-tl-corner = 1;
        wvous-tr-corner = 5;
      };
      NSGlobalDomain = {
        # 2 = heavy font smoothing; if text looks blurry, back this down to 1
        AppleFontSmoothing = 1;
        AppleShowAllExtensions = true;
        # Dark mode
        AppleInterfaceStyle = "Dark";
        # auto switch between light/dark mode
        AppleInterfaceStyleSwitchesAutomatically = true;
        "com.apple.sound.beep.feedback" = 1;
        "com.apple.sound.beep.volume" = 0.606531; # 50%
        "com.apple.mouse.tapBehavior" = null; # tap to click (null/1)
        "com.apple.swipescrolldirection" = true; # "natural" scrolling
        "com.apple.keyboard.fnState" = true;
        "com.apple.springing.enabled" = false;
        "com.apple.trackpad.scaling" = 0.6875; # fast
        "com.apple.trackpad.enableSecondaryClick" = true;
        # enable full keyboard control
        # (e.g. enable Tab in modal dialogs)
        AppleKeyboardUIMode = 3;
        #AppleMeasurementUnits = "Inches";
        # no popup menus when holding down letters
        ApplePressAndHoldEnabled = false;
        # delay before repeating keystrokes
        InitialKeyRepeat = 14;
        # delay between repeated keystrokes upon holding a key
        KeyRepeat = 1;
        AppleShowScrollBars = "Automatic";
        NSScrollAnimationEnabled = true; # smooth scrolling
        NSAutomaticCapitalizationEnabled = false;
        NSAutomaticDashSubstitutionEnabled = false;
        NSAutomaticPeriodSubstitutionEnabled = false;
        # no automatic smart quotes
        NSAutomaticQuoteSubstitutionEnabled = false;
        NSAutomaticSpellingCorrectionEnabled = false;
        NSNavPanelExpandedStateForSaveMode = true;
        NSNavPanelExpandedStateForSaveMode2 = true;
        NSDocumentSaveNewDocumentsToCloud = false;
        # speed up animation on open/save boxes (default:0.2)
        NSWindowResizeTime = 0.1;
        PMPrintingExpandedStateForPrint = true;
        PMPrintingExpandedStateForPrint2 = true;
      };
      alf = {
        # 0 = disabled 1 = enabled 2 = blocks all connections except for essential services
        globalstate = 1;
        loggingenabled = 0;
        stealthenabled = 1;
        allowdownloadsignedenabled = 1;
        allowsignedenabled = 1;
      };
    };
    keyboard = {
      enableKeyMapping = true;
      remapCapsLockToEscape = true;
    };

    # Many of these taken from https://github.com/mathiasbynens/dotfiles/blob/master/.macos
    activationScripts.extraActivation.enable = true;
    activationScripts.extraActivation.text = ''
      echo "Activating extra preferences..."
      # Close any open System Preferences panes, to prevent them from overriding
      # settings we’re about to change
      osascript -e 'tell application "System Preferences" to quit'
      defaults write com.apple.AdLib allowApplePersonalizedAdvertising -bool false
      # Increase window resize speed for Cocoa applications
      defaults write NSGlobalDomain NSWindowResizeTime -float 0.001
      # Automatically quit printer app once the print jobs complete
      defaults write com.apple.print.PrintingPrefs "Quit When Finished" -bool true
      # Use scroll gesture with the Ctrl (^) modifier key to zoom
      #defaults write com.apple.universalaccess closeViewScrollWheelToggle -bool true
      #defaults write com.apple.universalaccess HIDScrollZoomModifierMask -int 262144
      # Follow the keyboard focus while zoomed in
      #defaults write com.apple.universalaccess closeViewZoomFollowsFocus -bool true
      # Require password immediately after sleep or screen saver begins
       defaults write com.apple.screensaver askForPassword -int 1
       defaults write com.apple.screensaver askForPasswordDelay -int 0
      # Save screenshots to the desktop
      defaults write com.apple.screencapture location -string "~/gdrive/pictures/screenshots/"
      # Save screenshots in PNG format (other options: BMP, GIF, JPG, PDF, TIFF)
      defaults write com.apple.screencapture type -string "png"
      # Show icons for hard drives, servers, and removable media on the desktop
      defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
      defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true
      defaults write com.apple.finder ShowMountedServersOnDesktop -bool true
      defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool true
      defaults write com.apple.finder ShowStatusBar -bool true
      defaults write com.apple.finder ShowPathbar -bool true
      # Keep folders on top when sorting by name
      defaults write com.apple.finder _FXSortFoldersFirst -bool true
      # When performing a search, search the current folder by default
      defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"
      # Avoid creating .DS_Store files on network or USB volumes
      defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
      defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true
      # Use list view in all Finder windows by default
      # Four-letter codes for the other view modes: `icnv`, `clmv`, `glyv`
      defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"
      # Show the ~/Library folder
      #chflags nohidden ~/Library && xattr -d com.apple.FinderInfo ~/Library
      # Privacy: don’t send search queries to Apple
      defaults write com.apple.Safari UniversalSearchEnabled -bool false
      defaults write com.apple.Safari SuppressSearchSuggestions -bool true
      # Press Tab to highlight each item on a web page
      defaults write com.apple.Safari WebKitTabToLinksPreferenceKey -bool true
      defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2TabsToLinks -bool true
      # Show the full URL in the address bar (note: this still hides the scheme)
      defaults write com.apple.Safari ShowFullURLInSmartSearchField -bool true
      # Prevent Safari from opening ‘safe’ files automatically after downloading
      defaults write com.apple.Safari AutoOpenSafeDownloads -bool false
      # Disallow hitting the Backspace key to go to the previous page in history
      defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2BackspaceKeyNavigationEnabled -bool false
      # Hide Safari’s bookmarks bar by default
      defaults write com.apple.Safari ShowFavoritesBar -bool false
      # Enable Safari’s debug menu
      defaults write com.apple.Safari IncludeInternalDebugMenu -bool true
      # Enable the Develop menu and the Web Inspector in Safari
      defaults write com.apple.Safari IncludeDevelopMenu -bool true
      defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey -bool true
      defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2DeveloperExtrasEnabled -bool true
      # Add a context menu item for showing the Web Inspector in web views
      defaults write NSGlobalDomain WebKitDeveloperExtras -bool true
      # Enable continuous spellchecking
      defaults write com.apple.Safari WebContinuousSpellCheckingEnabled -bool true
      # Disable auto-correct
      defaults write com.apple.Safari WebAutomaticSpellingCorrectionEnabled -bool false
      # Disable AutoFill
      defaults write com.apple.Safari AutoFillFromAddressBook -bool false
      defaults write com.apple.Safari AutoFillCreditCardData -bool false
      defaults write com.apple.Safari AutoFillMiscellaneousForms -bool false
      # Warn about fraudulent websites
      defaults write com.apple.Safari WarnAboutFraudulentWebsites -bool true
      # Disable Java
      defaults write com.apple.Safari WebKitJavaEnabled -bool false
      defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2JavaEnabled -bool false
      defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2JavaEnabledForLocalFiles -bool false
      # Block pop-up windows
      defaults write com.apple.Safari WebKitJavaScriptCanOpenWindowsAutomatically -bool false
      defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2JavaScriptCanOpenWindowsAutomatically -bool false
      # Add the keyboard shortcut ⌘ + Enter to send an email in Mail.app
      defaults write com.apple.mail NSUserKeyEquivalents -dict-add "Send" "@\U21a9"
      # Display emails in threaded mode, sorted by date (newest at the top)
      defaults write com.apple.mail DraftsViewerAttributes -dict-add "DisplayInThreadedMode" -string "yes"
      defaults write com.apple.mail DraftsViewerAttributes -dict-add "SortedDescending" -string "no"
      defaults write com.apple.mail DraftsViewerAttributes -dict-add "SortOrder" -string "received-date"
      # Disable inline attachments (just show the icons)
      defaults write com.apple.mail DisableInlineAttachmentViewing -bool true
      defaults write com.apple.spotlight orderedItems -array \
      	'{"enabled" = 1;"name" = "APPLICATIONS";}' \
      	'{"enabled" = 1;"name" = "DIRECTORIES";}' \
      	'{"enabled" = 1;"name" = "PDF";}' \
      	'{"enabled" = 1;"name" = "DOCUMENTS";}' \
      	'{"enabled" = 1;"name" = "PRESENTATIONS";}' \
      	'{"enabled" = 1;"name" = "SPREADSHEETS";}' \
      	'{"enabled" = 1;"name" = "MENU_OTHER";}' \
      	'{"enabled" = 1;"name" = "CONTACT";}' \
      	'{"enabled" = 1;"name" = "IMAGES";}' \
      	'{"enabled" = 1;"name" = "MESSAGES";}' \
      	'{"enabled" = 1;"name" = "SYSTEM_PREFS";}' \
      	'{"enabled" = 1;"name" = "EVENT_TODO";}' \
      	'{"enabled" = 1;"name" = "MENU_CONVERSION";}' \
      	'{"enabled" = 1;"name" = "MENU_EXPRESSION";}' \
      	'{"enabled" = 0;"name" = "FONTS";}' \
      	'{"enabled" = 0;"name" = "BOOKMARKS";}' \
      	'{"enabled" = 0;"name" = "MUSIC";}' \
      	'{"enabled" = 0;"name" = "MOVIES";}' \
      	'{"enabled" = 0;"name" = "SOURCE";}' \
      	'{"enabled" = 0;"name" = "MENU_DEFINITION";}' \
      	'{"enabled" = 0;"name" = "MENU_WEBSEARCH";}' \
      	'{"enabled" = 0;"name" = "MENU_SPOTLIGHT_SUGGESTIONS";}'
      # Prevent Time Machine from prompting to use new hard drives as backup volume
      defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true
      # Enable the automatic update check
      defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true
      # Check for software updates daily, not just once per week
      defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1
      # Download newly available updates in background
      defaults write com.apple.SoftwareUpdate AutomaticDownload -int 1
      # Install System data files & security updates
      defaults write com.apple.SoftwareUpdate CriticalUpdateInstall -int 1
      # Turn on app auto-update
      defaults write com.apple.commerce AutoUpdate -bool true
      # Prevent Photos from opening automatically when devices are plugged in
      defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true
    '';
  };
  networking.knownNetworkServices = ["Wi-Fi"];
  documentation.enable = true;

  # Getting 'systemsetup[91256:1045834] ### Error:-99 File:/AppleInternal/Library/BuildRoots/97f6331a-ba75-11ed-a4bc-863efbbaf80d/Library/Caches/com.apple.xbs/Sources/Admin/InternetServices.m Line:379' error when enabled
  #time.timeZone = "Europe/Kyiv";
  # here go the darwin preferences and config items
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    enableBashCompletion = true;
  };

  environment = {
    shells = with pkgs; [bash zsh];
    loginShell = pkgs.zsh;
    systemPackages = with pkgs; [git curl coreutils gnused];
    pathsToLink = ["/Applications"];
    systemPath = ["/opt/homebrew/bin"];
  };

  nix = {
    #nixPath = ["darwin=/etc/${config.environment.etc.darwin.target}"];
    # manage nixbld users with nix darwin
    configureBuildUsers = true;
    gc = {
      automatic = true;
      options = "--delete-older-than 30d";
    };
    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
      experimental-features = nix-command flakes
      extra-platforms = x86_64-darwin aarch64-darwin
    '';
  };

  fonts.fontDir.enable = true; # DANGER
  fonts.fonts = with pkgs; [
    vegur
    noto-fonts
    vistafonts # msoffice prereq
    source-sans-pro
    (nerdfonts.override {
      fonts = [
        "FiraCode"
        "Hasklig"
        "DroidSansMono"
        "DejaVuSansMono"
        "iA-Writer"
        "JetBrainsMono"
        "Meslo"
        "SourceCodePro"
        "Inconsolata"
      ];
    })
  ];

  services = {
    nix-daemon.enable = true;
    # tiling manager
    yabai = {
      enable = true;
      config = {
        focus_follows_mouse = "autoraise";
        mouse_follows_focus = "off";
        window_placement = "second_child";
        window_opacity = "off";
        layout = "bsp";
        window_topmost = "on";
      };
      extraConfig = ''
        yabai -m rule --add app='System Preferences' manage=off
      '';
    };
    # Hotkey daemon
    skhd = {
      enable = true;
      skhdConfig = ''
        # Navigation
        alt - h : yabai -m window --focus west
        alt - j : yabai -m window --focus south
        alt - k : yabai -m window --focus north
        alt - l : yabai -m window --focus east

        # Moving windows
        shift + alt - h : yabai -m window --warp west
        shift + alt - j : yabai -m window --warp south
        shift + alt - k : yabai -m window --warp north
        shift + alt - l : yabai -m window --warp east

        # Move focus container to workspace
        shift + alt - m : yabai -m window --space last; yabai -m space --focus last
        shift + alt - p : yabai -m window --space prev; yabai -m space --focus prev
        shift + alt - n : yabai -m window --space next; yabai -m space --focus next
        shift + alt - 1 : yabai -m window --space 1; yabai -m space --focus 1
        shift + alt - 2 : yabai -m window --space 2; yabai -m space --focus 2
        shift + alt - 3 : yabai -m window --space 3; yabai -m space --focus 3
        shift + alt - 4 : yabai -m window --space 4; yabai -m space --focus 4

        # Resize windows
        lctrl + alt - h : yabai -m window --resize left:-50:0; \
                          yabai -m window --resize right:-50:0
        lctrl + alt - j : yabai -m window --resize bottom:0:50; \
                          yabai -m window --resize top:0:50
        lctrl + alt - k : yabai -m window --resize top:0:-50; \
                          yabai -m window --resize bottom:0:-50
        lctrl + alt - l : yabai -m window --resize right:50:0; \
                          yabai -m window --resize left:50:0

        # Equalize size of windows
        lctrl + alt - e : yabai -m space --balance

        # Enable / Disable gaps in current workspace
        lctrl + alt - g : yabai -m space --toggle padding; yabai -m space --toggle gap

        # Rotate windows clockwise and anticlockwise
        alt - r         : yabai -m space --rotate 270
        shift + alt - r : yabai -m space --rotate 90

        # Rotate on X and Y Axis
        shift + alt - x : yabai -m space --mirror x-axis
        shift + alt - y : yabai -m space --mirror y-axis

        # Set insertion point for focused container
        shift + lctrl + alt - h : yabai -m window --insert west
        shift + lctrl + alt - j : yabai -m window --insert south
        shift + lctrl + alt - k : yabai -m window --insert north
        shift + lctrl + alt - l : yabai -m window --insert east

        # Float / Unfloat window
        shift + alt - space : \
            yabai -m window --toggle float; \
            yabai -m window --toggle border

        # Restart Yabai
        shift + lctrl + alt - r : \
            /usr/bin/env osascript <<< \
                "display notification \"Restarting Yabai\" with title \"Yabai\""; \
            launchctl kickstart -k "gui/$UID/org.nixos.yabai"

        # Make window native fullscreen
        alt - f         : yabai -m window --toggle zoom-fullscreen
        shift + alt - f : yabai -m window --toggle native-fullscreen
      '';
    };
  };

  # Brew packages
  homebrew = {
    enable = true;
    caskArgs.no_quarantine = true;
    global.brewfile = true;
    onActivation = {
      autoUpdate = true;
      upgrade = true;
      cleanup = "uninstall";
    };
    masApps = {};
    taps = ["homebrew/bundle" "homebrew/services" "little-angry-clouds/homebrew-my-brews"];
    casks = [
      "raycast" # spotlight replacemenet
      # "amethyst" # window manager
      "monitorcontrol" # control brightness on multiple monitors
      "visual-studio-code" # code editor
      "vlc" # media player
      "brave-browser" # browser of choice
      "1password" # password manager
      "1password-cli" # cli for the 1password
    ];
    brews = [
      {
        name = "syncthing";
        restart_service = "changed";
        start_service = true;
      }
      "tfenv" # terraform versions manager
      "kbenv" # kubectl versions manager
      "kubent" # kube no trouble - check for depricated APIs
      "helmenv" # helm versions manager
      "opa" # open policy agent
      "iproute2mac" # ip routing utils
    ];
  };
}
