{pkgs, ...}: {
  # Brew packages
  homebrew = {
    masApps = {};
    taps = [];
    casks = [
      "steam"
      "anydesk"
      "multimc"
      "microsoft-remote-desktop"
      "balance-lock"
      "nvidia-geforce-now"
      "zerotier-one"
      "jellyfin-media-player"
      "tailscale"
    ];
    brews = [];
  };
}
