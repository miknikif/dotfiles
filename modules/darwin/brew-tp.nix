{pkgs, ...}: {
  # Brew packages
  homebrew = {
    masApps = {};
    taps = [];
    casks = [
      "docker"
      "zoom"
      "balance-lock"
      "signal"
      "grammarly-desktop"
      "vmware-fusion"
      "vagrant-vmware-utility"
      "openvpn-connect"
    ];
    brews = [
      "openjdk@17"
    ];
  };
}
