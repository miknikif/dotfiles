{pkgs, ...}: {
  # Brew packages
  homebrew = {
    masApps = {};
    taps = [];
    casks = [
      "clockify"
      "grammarly-desktop"
      "zoom"
      "utm"
    ];
    brews = [];
  };
}
