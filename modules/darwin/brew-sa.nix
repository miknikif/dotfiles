{pkgs, ...}: {
  # Brew packages
  homebrew = {
    masApps = {};
    taps = [];
    casks = [
      "slack"
      "amazon-workspaces"
      "mongodb-compass"
      "postman"
      "postman-cli"
    ];
    brews = [];
  };
}
